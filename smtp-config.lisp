(in-package :net.sinawali.stocking)

;; SMTP configurations
(defparameter *smtp-server* "")
;(defparameter *smtp-auth-user* "")
;(defparameter *smtp-pass* "")
(defparameter *smtp-port* 587)

;; Email configuration variables
(defparameter *email-from* "")
(defparameter *email-name* "Santa's Helper")
(defparameter *email-subject* "Secret Santa for ~A")
