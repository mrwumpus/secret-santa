(in-package :net.sinawali.stocking)

;;; Some configuration values
;;; Persistence
;;;
(defparameter *save-file* "stocking-state.lisp")
(defparameter *save-base* "stocking-state")
(defparameter *attachment-dir* #P"./attachments")

;;; Program State
;;;
(defvar *save-qualifier* "")
(defvar *stocking-list* nil)
(defvar *match-list* nil)
(defvar *needs-match* t)
(defvar *year-limit* 0)

;;; Let's create some classes
;;;
(defclass stuffer ()
  ((name :initarg :name :initform "" :reader name)
   (email :initarg :email :initform "" :reader email)
   (notes :initarg :notes :initform "Bah, Humbug!" :reader notes)))

;;; Function to make a new stuffer object
(defun make-stuffer (name email &optional (notes ""))
  (make-instance 'stuffer :name name :email email :notes notes))

(defun add-stuffer (name email &optional (notes ""))
  "Create and add a new stuffer to the global stuffer list"
  (let ((stuffer (make-stuffer name email notes)))
	(setf *stocking-list* (append *stocking-list* (list stuffer))))
  (reset-matches))

(defun range (num)
  "Create a list of integers from 0 to num"
  (let ((ret-list nil))
	(dotimes (x num)
		(push x ret-list))
	(nreverse ret-list)))

(defun reset-matches ()
  "Reset the matches list"
  (setf *match-list* (range (length *stocking-list*)))
  (setf *needs-match* t))

(defun gen-match-list (num)
  "Generate a random matches list"
  (if (< num 2) ()
    (handler-bind ((type-error
                     #'(lambda (c)
                         (declare (ignore c))
                         (invoke-restart 'do-over))))
      (labels ((match-list-rec (idx rem-list)
                               (cond
                                 ((or
                                    (null rem-list)
                                    (eq (length rem-list) 0))
                                  ())
                                 (t
                                   (let* ((sub-list (remove idx rem-list))
                                          (ret-idx (random (length sub-list)))
                                          (ret-num (nth ret-idx sub-list))
                                          (ret-list
                                            (match-list-rec (1+ idx)
                                                            (remove ret-num rem-list))))
                                     (if (null ret-list)
                                       (list ret-num)
                                       (push ret-num ret-list)))))))
        (loop
          (restart-case
            (return (match-list-rec 0 (range num)))
            (do-over () (print "DO OVER!"))))))))

(defun generate-matches ()
  "Generate matches when *needs-match* is true"
  (when *needs-match*
	(gen-matches)))

(defun gen-matches ()
  "Generate the matches list"
  (reset-matches)
  (setf *match-list* (gen-match-list (length *stocking-list*)))
  (setf *needs-match* nil))

(defun print-stocking-list (&key (notes nil))
  "Print the list of stuffer objects. Use the index value for specifying a single email"
	(map-stuffer-match (lambda (idx stuffer match)
                       (declare (ignore match))
		(format t "~A: ~A ~A~%~@[~A~%~%~]" (1+ idx) (name stuffer) (email stuffer) (and notes (notes stuffer))))))

(defun print-matches ()
  (map-stuffer-match
	(lambda (idx stuffer match)
		(format t "~A: ~A has ~A~%" (1+ idx) (name stuffer) (name match)))))

(defun map-stuffer-match (function)
  "Calls the function over the *stocking-list* with the index, stuffer and that stuffers match"
  (dotimes (x (length *stocking-list*))
	(let ((stuffer (nth x *stocking-list*))
			(match (nth (nth x *match-list*) *stocking-list*)))
		(funcall function x stuffer match))))

;;; Locate attachments
(defun get-attachments (email)
  "Find the attachements given the email"
   (let ((attachment-path
           (pathname
             (format nil "~A/~A/~A/*.*" *attachment-dir* *save-qualifier* email))))
    (directory attachment-path)))

(defun get-stuffer-attachments (num)
  (let* ((x (1- num))
		 (stuffer (nth x *stocking-list*)))
    (get-attachments (email stuffer))))

;;; Send the email
;;;
(defun send-email (to message attachments)
  "Actually send the email"
  ;; Can't get attachments to work properly...
  (declare (stuffer to))
  (let ((cl-smtp::*debug* nil)
        (cl+ssl:*make-ssl-client-stream-verify-default* nil))
  ;(format t "Sending email to ~A with match ~A.~%" (name to) (name match))
  (cl-smtp:send-email *smtp-server* *email-from* (email to) 
						(format nil *email-subject* (name to))
            message
						:port *smtp-port*
            :attachments attachments
            :display-name *email-name*
            :ssl :starttls
						:authentication (list *smtp-auth-user* *smtp-pass*)
						)))

(defun send-all-match-email ()
  (send-all-email #'generate-email-message t))

(defun send-all-welcome-email ()
    (send-all-email #'generate-welcome-message))

(defun send-all-email (msg-gen &optional (with-attachments? nil))
  "Send the emails to all people"
  (generate-matches)
  (flet ((send-email-int (idx to match)
                         (declare (ignore idx))
                        (let ((attachments
                                (when with-attachments?
                                  (get-attachments (email match)))))
						 (send-email to 
                (funcall msg-gen to match)
                attachments))))
    (map-stuffer-match #'send-email-int)))

(defun send-single-match-email (num)
  (send-single-email num #'generate-email-message t))

(defun send-single-by-match-email (num)
  (send-single-email-for-match num #'generate-email-message t))

(defun send-single-welcome-email (num)
    (send-single-email num #'generate-welcome-message))

(defun send-single-email (num msg-gen &optional (with-attachments? nil))
  "Send a single email, specifying the index from the stuffers list"
  (generate-matches)
  (let* ((x (1- num))
		 (stuffer (nth x *stocking-list*))
		 (match (nth (nth x *match-list*) *stocking-list*))
     (attachments
       (when with-attachments?
         (get-attachments (email match))))
     (msg (funcall msg-gen stuffer match)))
	(send-email stuffer msg attachments)))

(defun send-single-email-for-match (num msg-gen &optional (with-attachments? nil))
  "Send a single email to the secret santa, specifying the index from the stuffers list for who the match is."
  (let ((i (position (1- num) *match-list*)))
      (send-single-email (1+ i) msg-gen with-attachments?)))

;;; Utilities for generating email content
;;;
(defun generate-email-message-OLD (to match)
  (declare (stuffer to) (stuffer match))
  (format nil 
			"Welcome to the Fox family stocking exchange extravaganza. ~%This email is intended for ~A's eyes only. If you are not ~A, stop reading NOW!!!~%Scroll down a bit for your match.~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%Your stocking match is ~A.~%"
			(name to)
			(name to)
			(name match)))

(defun generate-email-message (to match)
  (declare (stuffer to) (stuffer match))
  (format nil 
          "It's that time of year again, time for Secret Santa!~%~%This year, the Secret Santa includes the following participants:~%~%~A~%~%Gifts are not to exceed $~:D.~%~%This email is intended for ~A's eyes only. If you are not ~A, stop reading NOW!!!~%~%Scroll down a bit for your match.~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%~%Your stocking match is ~A.~%A special note from ~A:~%~%~A"
      (generate-participant-list)
      *year-limit*
			(name to)
			(name to)
			(name match)
			(name match)
			(notes match)))

(defun generate-participant-list ()
  (format nil "~{~A~^, ~}" (mapcar #'name *stocking-list*)))

(defun generate-welcome-message (to match)
  (declare (ignore to match))
  (format nil
          "It's that time of year again! This year, the Secret Santa includes the following participants:~%~%~A~%~%Gifts are not to exceed $~:D.~%~%If you would like to include a comment and provide recommendations or even images to your Secret Santa, please reply to this email with what you'd like to say and attach any relevant images.~%~%The matches will go out within a couple of weeks, but don't worry, they can be resent once you send in recommendations.~%~%~~Santa's Helper"
          (generate-participant-list)
          *year-limit*))

;;; Functions to save and restore state
;;;
(defun save-state ()
  "Saves the state of the application. Use 'restore-state' to restore"
  (let ((*save-file* (format nil "~A.~A.lisp" *save-base* *save-qualifier*)))
    (with-open-file (stream *save-file* :direction :output :if-exists :supersede)
    (princ "(setf stocking::*stocking-list* " stream)
    (princ "(list " stream)
    (dolist (stuffer *stocking-list*)
      (format stream "~%(stocking::make-stuffer \"~A\" \"~A\" \"~A\")"
          (name stuffer)
          (email stuffer)
          (notes stuffer)))
    (format stream "))~%")
    (format stream "(setf stocking::*year-limit* ~D)" *year-limit*)
    (format stream "; {{{~%")
    (princ "(setf stocking::*match-list* " stream)
    (when (not (null *match-list*))
      (princ "'" stream))
    (princ *match-list* stream)
    (format stream ")~%")
    (format stream "; }}}~%; vim: set fdm=marker:"))
    (values)))

(defun restore-state (qualifier)
  "Restores the application state from the file"
  (setf *save-qualifier* qualifier)
  (let ((*save-file* (format nil "~A.~A.lisp" *save-base* qualifier)))
    (load *save-file*)
    (setf *needs-match* nil)))

