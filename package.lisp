(in-package :cl-user)

(defpackage :net.sinawali.stocking
  ;(:use :cl :wali-util)
  (:use :cl)
  (:nicknames :stocking)
  (:export :send-email
		   :add-stuffer
		   :save-state
		   :restore-state
		   :send-all-email
		   :send-single-email
		   :print-stocking-list))
