(asdf:defsystem stocking
	:author "Joe Fox"
	:version "0.1"
	:serial t
	:depends-on (:cl-smtp)
	:components (
    (:file "package")
    (:file "smtp-config")
    (:file "stocking")))
